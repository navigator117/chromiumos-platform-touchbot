# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
import numpy as np
import skimage.morphology as morphology
import skimage.transform as transform
import tempfile


from optofidelity.videoproc import (Canvas, FileVideoReader, Filter, Shape,
                                    VideoException, VideoWriter, Viewer)
from optofidelity.detection import Event, LEDDetector, Trace, VideoProcessor


class LEDCalibration(object):
  base_record_duration = 200
  """Duration at beginning of each recording (in number of frames)."""

  per_tap_record_duration = 160
  """Duration of each tap in recording (in number of frames)."""

  num_measurements = 2
  """Number of tap latency measurements (i.e. number of taps)."""

  def __init__(self):
    self.finger_down_delays = []
    self.finger_up_delays = []
    self.finger_down_delay = 0
    self.finger_up_delay = 0
    self.video = None

  def __getstate__(self):
    return (
        self.finger_down_delays,
        self.finger_up_delays,
        self.finger_down_delay,
        self.finger_up_delay
    )

  def __setstate__(self, state):
    (
        self.finger_down_delays,
        self.finger_up_delays,
        self.finger_down_delay,
        self.finger_up_delay
    ) = state
    self.video = None

  def Calibrate(self, dut, camera, debug):
    self.RecordVideo(dut, camera)
    trace = self.ProcessVideo(debug)
    self.ProcessTrace(trace, debug)

  def RecordVideo(self, dut, camera):
    duration = (self.base_record_duration +
                self.per_tap_record_duration * self.num_measurements)
    camera.PrepareRecording(duration)
    dut.Move(dut.width / 2, dut.height / 2, 10, blocking=True)
    camera.Trigger()
    dut.Tap(dut.width / 2, dut.height / 2, count=self.num_measurements)
    camera.WaitUntilReady()
    self.video = camera.GetVideoReader()

  def ProcessVideo(self, debug, video=None):
    video = video or self.video
    assert video, "No calibration video"

    trace = Trace()
    processor = VideoProcessor(None, debug)
    processor.AddDetector(LEDDetector())
    processor.Initialize(video, debug)
    return processor.ProcessVideo(video, debug)

  def ProcessTrace(self, trace, debug):
    if debug["trace"]:
      print trace
    led_events = trace.Events()
    i = 0
    while i + 3 < len(led_events):
      # We are looking for 2 LEDs turning on and off
      first_on = led_events[i]
      second_on = led_events[i + 1]
      first_off = led_events[i + 2]
      second_off = led_events[i + 3]

      # Make sure we got the right event types. If not the LED detection has
      # missed an LED event.
      assert first_on.event_type == Event.LED_ON
      assert second_on.event_type == Event.LED_ON
      assert first_off.event_type == Event.LED_OFF
      assert second_off.event_type == Event.LED_OFF

      # Calculate latency.
      self.finger_down_delays.append(second_on.time - first_on.time)
      self.finger_up_delays.append(second_off.time - first_off.time)
      i += 4

    self.finger_down_delay = np.mean(np.asarray(self.finger_down_delays))
    self.finger_up_delay = np.mean(np.asarray(self.finger_up_delays))


class CameraCalibration(object):
  """Detects the test area on a device.

  This class can detect the flashing test area and can rectify images
  to show only this area.
  """

  rect_scaling = 1.2
  """Factor by which the rectified image will be enlarged to make sure we are
     not losing any information due to interpolation."""

  test_area_min_area = 5000
  """Minimum area of a test area in square pixels."""

  test_area_min_solidity = 0.9
  """Minimum solidity of the test_area."""

  test_area_closing_kernel = morphology.disk(2)
  """Size of the disk shaped kernel used for the closing operation to close
     any holes in the detected shapes that might be caused by noise."""

  test_area_margin = 4
  """The test area is reduced in size by this margin to account for potential
     camera shake."""

  diff_threshold = 0.2
  """Threshold used on the difference between the on and off image to find
     the test area."""

  out_of_view_edge_pixels = 10
  """How many pixels of the test_area have to be at the edge of the screen
     to consider it being out of view?"""

  brightness_error_margin = 0.1
  """If the normalized brightness exceeds this threshold above 1 or below 0,
     the brightness is considered to be out of range and will cause an error."""

  max_edge_deviation = 0.5
  """When the bezel enters the test_area, the brightness along this edge will
     deviate from others. This threshold describes by how much it can deviate
     before causing an error."""

  diff_unchanged_max_mid_range = 0.01
  """If the mid-range value of a diff image exceeds this value it is considered
     to show no change."""

  reference_min_num_frames = 6
  """Minimum number of frames to use for building a reference frame."""

  min_dynamic_range = 0.3
  """Minimum range between black and white."""

  diff_change_min_duration = 2
  """Minimum number of frames that have to show change to accept it as as
     changing."""

  def __init__(self):
    self.shape = None
    self.rect_transform = None
    self.on_image = None
    self.on_profile = None
    self.off_image = None
    self.off_profile = None

    self.video = None
    self.video_cache_file = None

  def Calibrate(self, app, camera, debug):
    self.RecordVideo(app, camera)
    self.Process(debug)

  def CacheVideo(self):
    """Download video from the camera to a temporary file."""
    if isinstance(self.video, FileVideoReader):
      return
    self.video_cache_file = tempfile.NamedTemporaryFile(suffix=".mov")
    print "Downloading calibration video"
    writer = VideoWriter(self.video_cache_file.name)
    writer.Write(self.video)
    self.video = FileVideoReader(self.video_cache_file.name)

  def RecordVideo(self, app, camera):
    """Record calibration video on the app using the specified camera."""
    camera.PrepareRecording(60)
    app.EnterPage(app.PAGE_FLASH)
    app.dut.ClearScreen(blocking=True)
    camera.Trigger()
    camera.WaitUntilReady()
    app.ExitPage()
    self.video = camera.GetVideoReader()

  def EstimateRectTransform(self):
    """Estimates transformation from rectified device screen to video frame.

    Since transform.warp is doing inverse transformations, this matrix
    transform object can be used to rectify the screen in the video frame.
    """
    # Approximate rectangle from screen shape.
    dest_coords = self.shape.ApproximatePolygon(4)
    # Coords for transforms are x/y, when indexing y/x. So we have to switch.
    dest_coords = dest_coords[:, ::-1]

    # Coordinates in target image. Slightly inflates size so we are not losing
    # any information during warping.
    height = int(self.shape.height * self.rect_scaling)
    width = int(self.shape.width * self.rect_scaling)
    source_coords = np.array((
        (0, 0),
        (width, 0),
        (width, height),
        (0, height)
    ))
    tform = transform.ProjectiveTransform()
    tform.estimate(source_coords, dest_coords)
    return tform

  def Rectify(self, image):
    """Rectify the device screen from a video frame.

    The resulting image will show only the screen, warped to be rectangular.
    """
    height = int(self.shape.height * self.rect_scaling)
    width = int(self.shape.width * self.rect_scaling)
    res = transform.warp(image, self.rect_transform,
                         output_shape=(height, width))
    return res.astype(image.dtype)

  def Unrectify(self, image=None, coords=None):
    """Reverse rectification from device screen image into a video frame.

    The resulting image will show only the screen, warped to be rectangular.
    """
    if image is not None:
      res = transform.warp(image, self.rect_transform.inverse,
                           output_shape=self.shape.mask.shape)
      return res.astype(image.dtype)
    if coords is not None:
      res = self.rect_transform(coords)
      return res[0, 0], res[0, 1]

  def FindReferenceFrames(self, video_reader, debug):
    """Find black and white reference frames in a video of the flashing screen.

    This method averages multiple frames of the video showing the black and
    white screen, returning a black and white reference frame.

    Args:
      video_reader: A VideoReader instance.
      debug: A ProcessorDebugger instance.
    """
    ref_images = []
    with video_reader.PrefetchEnabled():
      frames = video_reader.Frames()

      # Accumulator to calculate mean images.
      i, prev_frame = frames.next()
      mean_accum = np.zeros(prev_frame.shape)
      mean_accum_num = 0
      last_frame_below_thresh = 0

      for i, frame in frames:
        # Calculate the mid-range value of the inter-frame difference.
        # This reliably describes the general direction of change.
        diff = frame - prev_frame
        midrange = Filter.StableMidRange(diff)

        if np.abs(midrange) < self.diff_unchanged_max_mid_range:
          # Add frame to accumulator
          mean_accum += frame
          mean_accum_num += 1
          last_frame_below_thresh = i
        elif i - last_frame_below_thresh > self.diff_change_min_duration:
          # The screen is in the process of switching colors.
          if mean_accum_num > self.reference_min_num_frames:
            # We collected enough samples for a solid reference image.
            ref_images.append(mean_accum / mean_accum_num)
            if len(ref_images) >= 2:
              break
          # Reset mean accumulator
          mean_accum_num = 0
          mean_accum = np.zeros(prev_frame.shape)

        if debug:
          debug.Print("%4d: midrange=%.2f, N=%d", i, midrange, mean_accum_num)
          Viewer.VideoFrame(0.5 + diff)
        prev_frame = frame

    # Add the last accumulation in case we don't have enough reference images
    if mean_accum_num > self.reference_min_num_frames and len(ref_images) < 2:
      ref_images.append(mean_accum / mean_accum_num)

    if len(ref_images) < 2:
      raise VideoException("Cannot find flashing screen")

    # Return (white_ref, black_ref)
    if np.mean(ref_images[0]) > np.mean(ref_images[1]):
      return ref_images[0], ref_images[1]
    else:
      return ref_images[1], ref_images[0]

  def Process(self, debug, video=None):
    """Process calibration video recorded by RecordVideo."""
    video = video or self.video
    assert video, "No calibration video"

    # Extract information from video frames. We need the following information:
    # Image of the screen off (min average grayscale)
    # Image of the screen on (max average grayscale)
    # And the accumulative difference between frames to find the area that
    # is changing the most.
    on_frame, off_frame = self.FindReferenceFrames(video, debug=debug)
    delta = Filter.Truncate(on_frame - off_frame)

    # Find screen shape
    margin_kernel = morphology.disk(self.test_area_margin)
    binary = delta > self.diff_threshold
    binary = morphology.binary_closing(binary, self.test_area_closing_kernel)
    binary = morphology.binary_erosion(binary, margin_kernel)
    shapes = Shape.Shapes(binary)
    shapes = [s for s in shapes
                if (s.area > self.test_area_min_area and
                    s.props.solidity > self.test_area_min_solidity)]
    if not shapes:
      raise VideoException("Cannot find screen shape")
    self.shape = max(shapes, key=lambda s: s.area)

    # create a 1px rectangle mask along the edge of the video frame
    # if the screen mask overlaps in too many pixels with this rectangle,
    # the screen is probably outside the viewport of the camera.
    width, height = self.shape.mask.shape
    frame_border = np.zeros((width, height), dtype=np.bool)
    frame_border[:, 0] = True
    frame_border[:, height - 1] = True
    frame_border[0, :] = True
    frame_border[width - 1, :] = True
    border_overlap = np.sum(frame_border * self.shape.mask)
    debug.Print("border_overlap=%.2f", border_overlap)

    if border_overlap > self.out_of_view_edge_pixels:
      raise VideoException("Screen out of camera view")

    # Estimate rectification transformation
    self.rect_transform = self.EstimateRectTransform()

    # Calculate profile for screen on and off in cropped space
    self.on_image = self.Rectify(on_frame)
    self.off_image = self.Rectify(off_frame)
    self.on_profile = Filter.ExtractProfile(self.on_image)
    self.off_profile = Filter.ExtractProfile(self.off_image)

    # Check dynamic range. We need enough resolution to distinguish shades
    # between black and white during later processing.
    dynamic_range = np.mean(self.on_image - self.off_image)
    debug.Print("dynamic_range=%.2f", dynamic_range)
    if dynamic_range <  self.min_dynamic_range:
      raise VideoException("Not enough dynamic range.")

    if debug:
      res = Canvas(on_frame)
      res.DrawContour(Canvas.BLUE, self.shape)
      Viewer.DebugView(delta=delta,
                       shape=res.image,
                       rectified_on=self.on_image,
                       rectified_off=self.off_image)
    return True

  def NormalizeFrame(self, rectified_frame):
    return (rectified_frame - self.off_image) / (self.on_image - self.off_image)

  def NormalizedBrightness(self, rectified_frame, slice_or_mask=None):
    """Returns normalized average brightness of frame.

    The brightness is normalized to 0 being the average brightness of the
    screen being black and 1 the average brightness of a fully white screen.
    """
    if slice_or_mask is not None:
      mean = np.mean(rectified_frame[slice_or_mask])
      on_mean = np.mean(self.on_image[slice_or_mask])
      off_mean = np.mean(self.off_image[slice_or_mask])
    else:
      mean = np.mean(rectified_frame[:])
      on_mean = np.mean(self.on_image[:])
      off_mean = np.mean(self.off_image[:])
    return (mean - off_mean) / (on_mean - off_mean)

  def VerifyFrame(self, frame, debug=False):
    """Check frame for camera shifts or lighting changes."""
    rectified = self.Rectify(frame)
    width, height = rectified.shape

    # calculate overall mean and mean along screen edges
    mean = self.NormalizedBrightness(rectified)
    top_mean = self.NormalizedBrightness(rectified, np.s_[:, 0])
    left_mean = self.NormalizedBrightness(rectified, np.s_[0, :])
    bottom_mean = self.NormalizedBrightness(rectified, np.s_[:, height - 1])
    right_mean = self.NormalizedBrightness(rectified, np.s_[width - 1, :])
    edge_means = [top_mean, left_mean, bottom_mean, right_mean]
    debug.Print("mean=%.2f", mean)
    debug.Print("edge_means=%s", edge_means)

    # The brightness is normalized to the black screen being 0 and the white
    # screen being 1. If the lighting changed, the normalized brightness will
    # be outside the [0, 1] range.
    error_margin = self.brightness_error_margin
    if mean > 1 + error_margin or mean < -error_margin:
      raise VideoException("Lighting changed.")

    # If the bezel shifted into the screen the deviation of the edge means
    # from the overall mean will be strong.
    deviations = np.abs(edge_means - mean)
    debug.Print("deviations=%s", deviations)
    if np.any(deviations > self.max_edge_deviation):
      raise VideoException("Camera shifted.")
